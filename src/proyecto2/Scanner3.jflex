package proyecto2;
import java_cup.runtime.Symbol;
import java.util.ArrayList;


%%

%cupsym tabla_simbolos3
%class Scanner3
%cup
%public
%unicode
%line
%column
%char
%ignorecase

%init{ /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
%init}

%{
public static ArrayList tokensList;
%}

parametro = [A-Z][0-9]+ |[0-9]+|[\-][0-9]+
nombre = [0-9] | [0-9][0-9]
coma = [\,]
%%

/*PALABRAS RESERVADAS*/

"Suma" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tsuma, yycolumn,yyline, new String(yytext()));}

"Resta" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tresta, yycolumn,yyline, new String(yytext()));}

"Multiplicacion" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tmultiplicacion, yycolumn,yyline, new String(yytext()));}

"Division" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tdivision, yycolumn,yyline, new String(yytext()));}

"\(" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tparentesisa, yycolumn,yyline, new String(yytext()));}

"\)" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tparentesisc, yycolumn,yyline, new String(yytext()));}

"\{" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tllavea, yycolumn,yyline, new String(yytext()));}

"\}" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tllavec, yycolumn,yyline, new String(yytext()));}

":" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tdospuntos, yycolumn,yyline);}

{coma} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tcoma, yycolumn,yyline);}

/*LETRAS*/
{nombre} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.ername, yycolumn,yyline, new String(yytext()));}

{parametro} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.erparametro, yycolumn,yyline, new String(yytext()));}


/* CUALQUIER OTRO */
[ \t\r\f\n]+             { /* se ignoran  */}
.                       {System.out.println("Error léxico: "+yytext());}